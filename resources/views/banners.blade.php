<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Home | UniquePrint</title>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/font-awesome.min.css" rel="stylesheet">
	<link href="../css/prettyPhoto.css" rel="stylesheet">
	<link href="../css/price-range.css" rel="stylesheet">
	<link href="../css/animate.css" rel="stylesheet">
	<link href="../css/main.css" rel="stylesheet">
	<link href="../css/responsive.css" rel="stylesheet">
	<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
	<link rel="shortcut icon" href="images/ico/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
	<header id="header">
		<!--header-->
		<div class="header_top">
			<!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li>
									<a href="#">
										<i class="fa fa-phone"></i> +2328187472197</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-envelope"></i> info@uniqueprint.com</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li>
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-linkedin"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-dribbble"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-google-plus"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header_top-->

		<div class="header-middle">
			<!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<h2>
								<a href="../index.html" style="text-decoration:none;color:#696763;">
									<span style="color:#FE980F;">Unique</span>Print</a>
							</h2>
						</div>
						<div class="btn-group pull-right">
							<!--<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canada</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canadian Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>-->
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<!--<li><a href="#"><i class="fa fa-user"></i> Account</a></li>
								<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>-->
								<li>
									<a href="../checkout.html">
										<i class="fa fa-crosshairs"></i> Checkout</a>
								</li>
								<li>
									<a href="../cart.html">
										<i class="fa fa-shopping-cart"></i> Cart</a>
								</li>
								<!--<li><a href="login.html"><i class="fa fa-lock"></i> Login</a></li>-->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-middle-->

		<div class="header-bottom">
			<!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li>
									<a href="index.html" class="active">Home</a>
								</li>
								<li class="dropdown">
									<a href="#">Shop
										<i class="fa fa-angle-down"></i>
									</a>
									<ul role="menu" class="sub-menu">
										<li>
											<a href="../shop.html">Products</a>
										</li>
										<li>
											<a href="../product-details.html">Product Details</a>
										</li>
										<li>
											<a href="../checkout.html">Checkout</a>
										</li>
										<li>
											<a href="../cart.html">Cart</a>
										</li>
										<li>
											<a href="../login.html">Login</a>
										</li>
									</ul>
								</li>

								<li>
									<a href="../contact-us.html">Contact</a>
								</li>
								<li>
									<a href="../contact-us.html">About</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-bottom-->
	</header>
	<!--/header-->



	<section>
		<div class="container">
			<div class="row">


				<div class="col-sm-12 padding-right">
					<div class="features_items">
						<!--features_items-->
						<h2 class="title text-center">Banners</h2>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="../images/banners/roll_up_banner.jpg" alt="" />
										<h2>Roll up Banners</h2>
										<p>Ideal for conferences, promotional events, trade expos and displays in your own store,
                                            our high-quality roll-up banner will help you increase your brand's visibility. 
                                            Comes with retractable stands and carrier bags.
										</p>

										<p>
											<b>Material</b> - Full colour digital printing on PVC material. Size: 33 x 81 inches
										</p>

										<p>
											<b>Finishing</b>-Printing and mounted on Rollup Banner stand
										</p>
										<p id="prices">Starting @
											<span>&#8358;</span>3,500</p>
										<br/>
										<a href="" data-toggle="modal" data-target="#myModal">
											<button id="order" type="submit"class="btn btn-danger submit" name="submit" value="submit">Order Now</button>
										</a>
										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="../images/banners/x_banner.jpg" alt="" />
										<h2>X-Banner</h2>

										<p>
                                                X-Banners are the budget-friendly way to give your business the exposure it deserves. 
                                                Use at conferences, events, trade expos or displays in your own store. 
                                                Customized with your logo and marketing messages

										</p>
										<p>
											<b>Material</b> - 350gsm matte card (4"x 6" post card size) 
										</p>

										<p>
											<b>Finishing</b>-  Matte lamination finish with round corner options
										</p>
										<p id="prices">Starting @
											<span>&#8358;</span>3,500</p>
										<br/>
										<a href="" data-toggle="modal" data-target="#myModal">
												<button id="order" type="submit"class="btn btn-danger submit" name="submit" value="submit">Order Now</button>
											</a>

										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="../images/banners/flex_banner.png" alt="" />
										<h2>Flex Banners</h2>
										<p>
                                                Order top quality flex banners for your business. 
                                                Give your business the attention it deserves.
                                                 Flex banners are also perfect for your events.
										</p>
										<p>
											<b>Material</b> -   Top Quality Flex Banner Material

										</p>

										<p>
											<b>Finishing</b>-  7ft by 3ft with grommets (eyelets) on corners for hanging and diaply7
										</p>
										<p id="prices">Starting @
											<span>&#8358;</span>3,500</p>
										<br/>
										<a href="" data-toggle="modal" data-target="#myModal">
												<button id="order" type="submit" class="btn btn-danger submit" name="submit" value="submit">Order Now</button>
											</a>
									</div>

								</div>

							</div>
						</div>



					</div>
					<!--features_items-->


					


				</div>
			</div>
		</div>
	</section>

	<footer id="footer">
		<!--Footer-->
		<div class="footer-top">
			<div class="container">
					<div class="row">
							<div class="col-sm-2">
								<div class="companyinfo">
									<h2>
										<span>UNIQUE</span>PRINT</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
								</div>
							</div>
							<div class="col-sm-7">
									<div class="col-sm-4">
										<p>
											<center><img src="../images/fast.png" class="imgs" />
											</center>
			
			
									</div>
			
									<div class="col-sm-4">
										<p>
											<center><img src="../images/nationwide.png" class="imgs" />
											</center>
									</div>
			
									<div class="col-sm-4">
										<p>
											<center><img src="../images/satisfactory.png" class="imgs"  />
										</center>
									</div>
								</div>
							<div class="col-sm-3">
								<div class="address">
									<img src="../images/home/map.png" alt="" />
									<p>Old Garage, Off Oba-Adesida Road, Akure. Ondo State. Nigeria</p>
								</div>
							</div>
						</div>
			</div>
		</div>

		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Online Help</a>
								</li>
								<li>
									<a href="#">Contact Us</a>
								</li>
								<li>
									<a href="#">Order Status</a>
								</li>
								<li>
									<a href="#">Change Location</a>
								</li>
								<li>
									<a href="#">FAQ’s</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quick Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Banners</a>
								</li>
								<li>
									<a href="#">Handbills</a>
								</li>
								<li>
									<a href="#">Posters</a>
								</li>
								<li>
									<a href="#">Stickers</a>
								</li>
								<li>
									<a href="#">Branding </a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Terms of Use</a>
								</li>
								<li>
									<a href="#">Privecy Policy</a>
								</li>
								<li>
									<a href="#">Refund Policy</a>
								</li>
								<li>
									<a href="#">Billing System</a>
								</li>
								<li>
									<a href="#">Ticket System</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Company Information</a>
								</li>
								<li>
									<a href="#">Careers</a>
								</li>
								<li>
									<a href="#">Store Location</a>
								</li>
								<li>
									<a href="#">Affillate Program</a>
								</li>
								<li>
									<a href="#">Copyright</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>About UniquePrint</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default">
									<i class="fa fa-arrow-circle-o-right"></i>
								</button>
								<p>Get the most recent updates from
									<br />our site and be updated your self...</p>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>



	</footer>
	<!--/Footer-->

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog modal-sm">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" style="text-align:center;">How Would You love to Order?</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<a href="links/modal/upload.html">
								<div class="col-sm-4">
									<img src="../images/upload_design.png" height="100px" width="100px" ;/>
									<br />
									<br/>
									<p style="text-align:justify; font-size:12px; margin-left: 10px;">Browse our designs and customize
										<br />to your taste</p>

								</div>
							</a>

							<div class="col-sm-4">
								<img src="../images/experts.png" height="100px" width="100px" />
								<br />
								<br/>
								<p style="text-align:justify; font-size:12px;">Upload your complete design
									<br />(AI, PDF, PSD, CDR, JPEG, PNG)</p>

							</div>

							<div class="col-sm-4">
								<img src="../images/browse_designs.png" height="100px" width="100px" />
								<br />
								<br/>
								<p style="text-align:justify; font-size:12px;">Let our experts help you with design.</p>

							</div>
						</div>


					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>




	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/jquery.scrollUp.min.js"></script>
	<script src="../js/price-range.js"></script>
	<script src="../js/jquery.prettyPhoto.js"></script>
	<script src="../js/main.js"></script>
</body>

</html>
