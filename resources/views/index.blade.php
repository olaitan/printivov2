<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Home | UniquePrint</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/price-range.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
	<link rel="shortcut icon" href="images/ico/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
	<header id="header">
		<!--header-->
		<div class="header_top">
			<!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li>
									<a href="#">
										<i class="fa fa-phone"></i> +2328187472197</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-envelope"></i> info@uniqueprint.com</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li>
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-linkedin"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-dribbble"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-google-plus"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header_top-->

		<div class="header-middle">
			<!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<h2>
								<a href="index.html" style="text-decoration:none;color:#696763;">
									<span style="color:#FE980F;">Unique</span>Print</a>
							</h2>
						</div>
						<div class="btn-group pull-right">
							<!--<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canada</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canadian Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>-->
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<!--<li><a href="#"><i class="fa fa-user"></i> Account</a></li>
								<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>-->
								<li>
									<a href="checkout.html">
										<i class="fa fa-crosshairs"></i> Checkout</a>
								</li>
								<li>
									<a href="cart.html">
										<i class="fa fa-shopping-cart"></i> Cart</a>
								</li>
								<!--<li><a href="login.html"><i class="fa fa-lock"></i> Login</a></li>-->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-middle-->

		<div class="header-bottom">
			<!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li>
									<a href="index.html" class="active">Home</a>
								</li>
								<li class="dropdown">
									<a href="#">Shop
										<i class="fa fa-angle-down"></i>
									</a>
									<ul role="menu" class="sub-menu">
										<li>
											<a href="shop.html">Products</a>
										</li>
										<li>
											<a href="product-details.html">Product Details</a>
										</li>
										<li>
											<a href="checkout.html">Checkout</a>
										</li>
										<li>
											<a href="cart.html">Cart</a>
										</li>
										<li>
											<a href="login.html">Login</a>
										</li>
									</ul>
								</li>

								<li>
									<a href="contact-us.html">Contact</a>
								</li>
								<li>
									<a href="contact-us.html">About</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-bottom-->
	</header>
	<!--/header-->

	<section id="slider">
		<!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>

						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1>
										<span>UNIQUE</span>PRINT</h1>
									<h2>Your Number 1 online print shop</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="images/home/girl1.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1>
										<span>UNIQUE</span>PRINT</h1>
									<h2>Delivery is as fast as it gets</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="images/home/girl2.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div>

							<div class="item">
								<div class="col-sm-6">
									<h1>
										<span>UNIQUE</span>PRINT</h1>
									<h2>Nigeria's Online Print Shop</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="images/home/girl3.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div>

						</div>

						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!--/slider-->

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Graphics Design</h2>
						<div class="panel-group category-products" id="accordian">
							<!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right">
												<i class="fa fa-plus"></i>
											</span>
											Letters
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li>
												<a href="#">Nike </a>
											</li>
											<li>
												<a href="#">Under Armour </a>
											</li>
											<li>
												<a href="#">Adidas </a>
											</li>
											<li>
												<a href="#">Puma</a>
											</li>
											<li>
												<a href="#">ASICS </a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right">
												<i class="fa fa-plus"></i>
											</span>
											Business Cards
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li>
												<a href="#">Fendi</a>
											</li>
											<li>
												<a href="#">Guess</a>
											</li>
											<li>
												<a href="#">Valentino</a>
											</li>
											<li>
												<a href="#">Dior</a>
											</li>
											<li>
												<a href="#">Versace</a>
											</li>
											<li>
												<a href="#">Armani</a>
											</li>
											<li>
												<a href="#">Prada</a>
											</li>
											<li>
												<a href="#">Dolce and Gabbana</a>
											</li>
											<li>
												<a href="#">Chanel</a>
											</li>
											<li>
												<a href="#">Gucci</a>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#womens">
											<span class="badge pull-right">
												<i class="fa fa-plus"></i>
											</span>
											Logos
										</a>
									</h4>
								</div>
								<div id="womens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li>
												<a href="#">Fendi</a>
											</li>
											<li>
												<a href="#">Guess</a>
											</li>
											<li>
												<a href="#">Valentino</a>
											</li>
											<li>
												<a href="#">Dior</a>
											</li>
											<li>
												<a href="#">Versace</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">Greeting Cards</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">Stickers</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">Banners</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">Brochure</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">Calendars</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">Corporate Gifts</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">Phone Cases</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a href="#">Paper Bags</a>
										</h4>
									</div>
								</div>
								<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a href="#">Posters</a>
											</h4>
										</div>
									</div>
						</div>
						<!--/category-products-->

						<div class="brands_products">
							<!--brands_products-->
							<h2>Branding</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li>
										<a href="#">
											<span class="pull-right"></span>Calendar Brandings</a>
									</li>
									<li>
										<a href="#">
											<span class="pull-right"></span>Brochure Brandings</a>
									</li>
									<li>
										<a href="#">
											<span class="pull-right"></span>Letterhead Brandings</a>
									</li>
									<li>
										<a href="#">
											<span class="pull-right"></span>Shirt Brandings</a>
									</li>
									<li>
										<a href="#">
											<span class="pull-right"></span>Wall Arts Brandings</a>
									</li>
									<li>
										<a href="#">
											<span class="pull-right"></span>Note pads Brandings</a>
									</li>
									<li>
										<a href="#">
											<span class="pull-right"></span>Wedding Stationery</a>
									</li>
								</ul>
							</div>
						</div>
						<!--/brands_products-->



						<div class="shipping text-center">
							<!--shipping-->
							<img src="images/home/shipping.jpg" alt="" />
						</div>
						<!--/shipping-->

					</div>
				</div>

				<div class="col-sm-9 padding-right">
					<div class="features_items">
						<!--features_items-->
						<h2 class="title text-center">Products</h2>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<a href="links/business_cards.html">
										<div class="productinfo text-center">
											<img src="images/business_cards/business_card.jpg" alt="" />
											<h2>Business Cards</h2>
											<p>Starting @
												<span>&#8358;</span>3,500</p>

											<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
										</div>
									</a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<a href="links/letter_heads.html">
										<div class="productinfo text-center">

											<img src="images/letter_heads/letter_head.jpg" alt="" />
											<h2>Letterheads</h2>
											<p>Starting @
												<span>&#8358;</span>3,500</p>

											<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
										</div>
									</a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<a href="links/branded_envelopes.html">
										<div class="productinfo text-center">
											<img src="images/branded_envelopes/branded_envelope2.jpg" alt="" />
											<h2>Branded Envelopes</h2>
											<p>Starting @
												<span>&#8358;</span>3,500</p>
											<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
										</div>
									</a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<a href="links/greeting_cards.html">
										<div class="productinfo text-center">
											<img src="images/greeting_cards/greeting_card1.jpg" alt="" />
											<h2>Greeting Cards</h2>
											<p>Starting @
												<span>&#8358;</span>3,500</p>
											<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
										</div>
									</a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<a href="links/flyers.html">
									<div class="productinfo text-center">
										<img src="images/flyers/flyer1.jpg" alt="" />
										<h2>Flyers</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>

										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div></a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<a href="links/paper_bags.html">
									<div class="productinfo text-center">
											
										<img src="images/paper_bags/paper_bag.png" alt="" />
										<h2>Paper Bags</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>
										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div></a>

								</div>

							</div>
						</div>

						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<a href="links/wedding_stationery.html">
									<div class="productinfo text-center">

										<img src="images/wedding_stationery/wedding_stationery.jpg" alt="" />
										<h2>Wedding Stationery</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>

										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div></a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<a href="links/stickers.html"><div class="productinfo text-center">
										<img src="images/stickers/bumper_sticker.jpg" alt="" />
										<h2>Stickers</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>

										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div></a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<a href="links/branded_notepads.html"><div class="productinfo text-center">
										<img src="images/branded_notepads/branded_notepad.jpg" alt="" />
										<h2>Branded Notepads</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>
										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div></a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<a href="links/custom_mugs.html"><div class="productinfo text-center">
										<img src="images/custom_mugs/custom_mug.jpg" alt="" />
										<h2>Custom Mugs</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>
										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<a href="links/banners.html"><div class="productinfo text-center">
										<img src="images/banners/banner.jpg" alt="" />
										<h2>Banners</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>

										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div></a>

								</div>

							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<a href="links/posters.html"><div class="productinfo text-center">
										<img src="images/posters/a1_poster.png" alt="" />
										<h2>Posters</h2>
										<p>Starting @
											<span>&#8358;</span>3,500</p>
										<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
									</div></a>
								</div>

							</div>
						</div>



					</div>
					<!--features_items-->





				</div>
			</div>
		</div>
	</section>

	<footer id="footer">
		<!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2>
								<span>UNIQUE</span>PRINT</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-4">
							<p>
								<img src="images/fast.png" class="imgs" />



						</div>

						<div class="col-sm-4">
							<p>
								<img src="images/nationwide.png" class="imgs" />

						</div>

						<div class="col-sm-4">
							<p>
								<img src="images/satisfactory.png" class="imgs" />

						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>Old Garage, Off Oba-Adesida Road, Akure. Ondo State. Nigeria</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Online Help</a>
								</li>
								<li>
									<a href="#">Contact Us</a>
								</li>
								<li>
									<a href="#">Order Status</a>
								</li>
								<li>
									<a href="#">Change Location</a>
								</li>
								<li>
									<a href="#">FAQ’s</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quick Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Banners</a>
								</li>
								<li>
									<a href="#">Handbills</a>
								</li>
								<li>
									<a href="#">Posters</a>
								</li>
								<li>
									<a href="#">Stickers</a>
								</li>
								<li>
									<a href="#">Branding </a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Terms of Use</a>
								</li>
								<li>
									<a href="#">Privecy Policy</a>
								</li>
								<li>
									<a href="#">Refund Policy</a>
								</li>
								<li>
									<a href="#">Billing System</a>
								</li>
								<li>
									<a href="#">Ticket System</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="#">Company Information</a>
								</li>
								<li>
									<a href="#">Careers</a>
								</li>
								<li>
									<a href="#">Store Location</a>
								</li>
								<li>
									<a href="#">Affillate Program</a>
								</li>
								<li>
									<a href="#">Copyright</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>About UniquePrint</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default">
									<i class="fa fa-arrow-circle-o-right"></i>
								</button>
								<p>Get the most recent updates from
									<br />our site and be updated your self...</p>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>



	</footer>
	<!--/Footer-->



	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>

</html>